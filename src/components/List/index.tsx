import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";

import { Place } from "@material-ui/icons";

import Select from "../Select";

import useStyles from "./styles";
import { Props } from "./types";
import { Typography } from "@material-ui/core";

function List(props: Props) {
  const { list, drivers, vehicles, handleUpdate } = props;

  const styled = useStyles();

  const renderTableHead = () => (
    <TableHead>
      <TableRow>
        <TableCell>Nombre</TableCell>
        <TableCell align="center">Hora</TableCell>
        <TableCell align="center">Tiempo</TableCell>
        <TableCell align="center">
          <Place />
        </TableCell>
        <TableCell align="center">Acción</TableCell>
        <TableCell align="center">Asignación</TableCell>
      </TableRow>
    </TableHead>
  );

  const renderTableBody = () => (
    <TableBody>
      {list.map((row: any) => (
        <TableRow
          key={row.id}
          sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
          className={
            row.driver_id
              ? styled.tableBodyRouteAssigned
              : styled.tableBodyRoute
          }
        >
          <TableCell component="th" scope="row">
            Ruta {row.id}
          </TableCell>
          <TableCell align="center">{renderHour(row)}</TableCell>
          <TableCell align="center">{row.travel_time}</TableCell>
          <TableCell align="center">{row.total_stops}</TableCell>
          <TableCell align="center">{row.action}</TableCell>
          <TableCell align="center">{renderSelectDriver(row)}</TableCell>
        </TableRow>
      ))}
    </TableBody>
  );

  const renderHour = (row: any) => (
    <div>
      <input
        type="time"
        min="06:00"
        max="21:00"
        value={row.starts_at.split(" ")[1]}
      />
      -
      <input
        type="time"
        min="06:00"
        max="21:00"
        value={row.ends_at.split(" ")[1]}
      />
    </div>
  );

  const renderSelectDriver = (row: any) => (
    <Select
      key={row.id}
      list={drivers}
      vehicles={vehicles}
      selection={row.driver_id === null ? 0 : row.driver_id}
      handleSelect={(value) => {
        let route = {
          driver_id: value,
        };
        handleUpdate(row.id, route);
      }}
    />
  );

  const renderContentList = () =>
    list.length > 0 ? (
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }}>
          {renderTableHead()}
          {renderTableBody()}
        </Table>
      </TableContainer>
    ) : (
      <Typography>No existen rutas para esta organizacion</Typography>
    );

  return renderContentList();
}

export default List;
