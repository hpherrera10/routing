import { IDriver } from "../../common/@types/driver";
import { IVehicle } from "../../common/@types/vehicle";

export interface Props {
  list: any;
  drivers: IDriver[];
  vehicles: IVehicle[];
  handleUpdate: (routeId: number, route: any) => void;
}
