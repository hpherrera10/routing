import { Theme, createStyles, makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    select: {
      height: 40,
      marginLeft: 16,
    },
    tableBodyRouteAssigned: {
      borderRight: "4px solid red",
    },
    tableBodyRoute: {
      borderRight: "4px solid green",
    },
  })
);

export default useStyles;
