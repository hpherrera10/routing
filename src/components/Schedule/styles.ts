import { Theme, createStyles, makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    content: {
      marginTop: 32,
    },
    header: {
      backgroundColor: "grey",
    },
    sidebarTitle: {
      backgroundColor: "#F4F4F4",
      padding: 16,
    },
    tableBodyRouteAssigned: {
      borderRight: "4px solid red",
    },
    tableBodyRoute: {
      borderRight: "4px solid green",
    },
  })
);

export default useStyles;
