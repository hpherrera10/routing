import { IRoute } from "../../common/@types/route";

export interface Props {
  routes: IRoute[];
}
