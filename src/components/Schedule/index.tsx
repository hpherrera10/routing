import Timeline, {
  TimelineHeaders,
  SidebarHeader,
  DateHeader,
} from "react-calendar-timeline";
import "react-calendar-timeline/lib/Timeline.css";
import moment from "moment";

import { Props } from "./types";
import useStyles from "./styles";
import { RouteServ } from "../../services";

function Schedule(props: Props) {
  const { routes } = props;
  const groups: any = [];
  const items: any = [];
  const styled = useStyles();

  routes.forEach((route: any) => {
    groups.push({ id: route.id, title: "Ruta " + route.id });
    items.push({
      id: route.id,
      group: route.id,
      title: "Ruta " + route.id,
      start_time: new Date(route.starts_at).getTime(),
      end_time: new Date(route.ends_at).getTime(),
      itemProps: {
        style: {
          background: RouteServ.validateTimeCaps(route, routes),
        },
      },
    });
  });

  return (
    <div className={styled.content}>
      <Timeline
        groups={groups}
        items={items}
        defaultTimeStart={moment().add(-12, "hour")}
        defaultTimeEnd={moment().add(12, "hour")}
      >
        <TimelineHeaders>
          <SidebarHeader>
            {({ getRootProps }) => {
              return (
                <div {...getRootProps()} className={styled.sidebarTitle}>
                  Rutas
                </div>
              );
            }}
          </SidebarHeader>
          <DateHeader unit="primaryHeader" className={styled.header} />
          <DateHeader />
        </TimelineHeaders>
      </Timeline>
    </div>
  );
}

export default Schedule;
