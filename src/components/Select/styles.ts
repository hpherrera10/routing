import { Theme, createStyles, makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    select: {
      height: 40,
      marginLeft: 16,
      width: 280,
    },
  })
);

export default useStyles;
