import { MenuItem } from "@material-ui/core";
import Select from "@mui/material/Select";
import useStyles from "./styles";
import { Props } from "./types";

function InputSelect(props: Props) {
  const { list, selection, vehicles, handleSelect } = props;
  const styled = useStyles();

  const renderItem = (value: any) =>
    vehicles && vehicles?.length > 0 ? (
      <MenuItem key={value.id} value={value.id}>
        {value.name} - {vehicles?.find((v) => v.id === value.vehicle_id)?.plate}
      </MenuItem>
    ) : (
      <MenuItem key={value.id} value={value.id}>
        {value.name}
      </MenuItem>
    );

  return (
    <Select
      value={selection}
      onChange={(event) => handleSelect(event.target.value)}
      className={styled.select}
    >
      <MenuItem value={0}>Selecciona</MenuItem>
      {list.map((value: any) => renderItem(value))}
    </Select>
  );
}

export default InputSelect;
