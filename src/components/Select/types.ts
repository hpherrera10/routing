import { IVehicle } from "../../common/@types/vehicle";

export interface Props {
  list: any;
  selection: any;
  vehicles?: IVehicle[];
  handleSelect: (selection: any) => void;
}
