import { Button, Typography } from "@material-ui/core";
import useStyles from "./styles";

function Title() {
  const styled = useStyles();

  return (
    <div className={styled.title}>
      <Typography variant="h5"> Conjunto de rutas 19/01/22</Typography>
      <div>
        <Button variant="contained">Editar</Button>
        <Button variant="contained" color="primary" className={styled.button}>
          Enviar a conductores
        </Button>
      </div>
    </div>
  );
}

export default Title;
