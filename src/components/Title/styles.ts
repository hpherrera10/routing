import { Theme, createStyles, makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    title: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
    },
    button: {
      marginLeft: 16,
    },
  })
);

export default useStyles;
