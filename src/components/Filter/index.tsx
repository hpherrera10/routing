import { Button } from "@material-ui/core";
import useStyles from "./styles";

function Filter() {
  const styled = useStyles();

  return (
    <div className={styled.filter}>
      <Button variant="contained" color="primary">
        Todos
      </Button>
      <Button variant="contained" className={styled.button}>
        1
      </Button>
      <Button variant="contained" className={styled.button}>
        2
      </Button>
      <Button variant="contained" className={styled.button}>
        3
      </Button>
    </div>
  );
}

export default Filter;
