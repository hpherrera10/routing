import { Theme, createStyles, makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    button: {
      marginLeft: 16,
    },
    filter: {
      marginTop: 16,
      marginBottom: 32,
    },
  })
);

export default useStyles;
