import { StateProps, DispatchProps } from "../../screens/RouteList/types";
import { connect } from "react-redux";

import RouteList from "../../screens/RouteList/Screen";
import {
  getOrganization,
  getOrganizations,
} from "../../store/organization/actions";
import { getRoutes, putRoute } from "../../store/route/actions";
import { getDrivers } from "../../store/driver/actions";
import { getVehicles } from "../../store/vehicle/actions";

function mapStateToProps(state: any): StateProps {
  return {
    organizations: state.organization.organizations || [],
    organization: state.organization.organization,
    routes: state.route.routes || [],
    drivers: state.driver.drivers || [],
    loading: state.remote.loading.Organization,
    message: state.route.message,
    vehicles: state.vehicle.vehicles || [],
  };
}

function mapDispatchToProps(dispatch: any): DispatchProps {
  return {
    loadOrganizations() {
      dispatch(getOrganizations());
    },
    loadRoutes(orgId: number) {
      dispatch(getOrganization(orgId));
      dispatch(getRoutes(orgId));
    },
    loadVehicles(orgId: number) {
      dispatch(getVehicles(orgId));
    },
    loadDrivers(orgId: number) {
      dispatch(getDrivers(orgId));
    },
    handleUpdate(routeId: number, route: any) {
      dispatch(putRoute(routeId, route));
    },
  };
}

const ConnectedComponent = connect<StateProps, DispatchProps>(
  mapStateToProps,
  mapDispatchToProps
)(RouteList);

export default ConnectedComponent;
