import { Route, Routes } from "react-router-dom";
import RouteList from "./container/RouteList";

function Start() {
  return (
    <Routes>
      <Route path="/" element={<RouteList />} />
    </Routes>
  );
}

export default Start;
