import axios from "axios";

const API_URL = "http://localhost:5000/api/v1/organizations";

function getOrganizations() {
  return axios.get(API_URL).then((response) => {
    if (response) {
      return response.data;
    }
    return null;
  });
}

function getOrganization(orgId: number) {
  return axios.get(API_URL + "/" + orgId).then((response) => {
    if (response) {
      window.localStorage.setItem(
        "organization",
        JSON.stringify(response.data)
      );
      return response.data;
    }
    return null;
  });
}

export default {
  getOrganizations,
  getOrganization,
};
