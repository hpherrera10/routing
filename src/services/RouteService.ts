import axios from "axios";
import { IRoute } from "../common/@types/route";

const API_URL = "http://localhost:5000/api/v1/routes";

function getRoutes(orgId: number) {
  return axios
    .get(API_URL, {
      params: { organization_id: orgId },
    })
    .then((response) => {
      if (response) {
        return response.data;
      }
      return null;
    });
}

function putRoute(routeId: number, route: any) {
  return axios.put(API_URL + "/" + routeId, route).then((response) => {
    if (response) {
      return response.data;
    }
    return null;
  });
}

function validateTimeCaps(route: any, routes: any) {
  let color = "green";

  if (route.driver_id) {
    color = "red";

    routes.forEach((r: any) => {
      if (route.id !== r.id && route.driver_id === r.driver_id) {
        if (route.starts_at.split(" ")[0] === r.starts_at.split(" ")[0]) {
          if (
            route.starts_at.split(" ")[1] <= r.starts_at.split(" ")[1] &&
            route.ends_at.split(" ")[1] <= r.ends_at.split(" ")[1]
          ) {
            color = "grey";
          }
        }
      }
    });
  }

  return color;
}

export default {
  getRoutes,
  putRoute,
  validateTimeCaps,
};
