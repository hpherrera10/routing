import axios from "axios";

const API_URL = "http://localhost:5000/api/v1/drivers";

function getDrivers(org_id: number) {
  return axios
    .get(API_URL, {
      params: { organization_id: org_id },
    })
    .then((response) => {
      if (response) {
        return response.data;
      }
      return null;
    });
}

export default {
  getDrivers,
};
