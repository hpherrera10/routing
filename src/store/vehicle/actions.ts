import { createAction, remoteCall } from "../utils";
import { VehicleServ } from "../../services";
import { IDriver } from "../../common/@types/driver";

export const getVehiclesSuccess = createAction("vehicle/getVehiclesSuccess");
export const getVehicleError = createAction("vehicle/getVehiclesError");

export const getVehicles = (orgId: number) => {
  return (dispatch: any) =>
    remoteCall(dispatch, "Vehicle", () => VehicleServ.getVehicles(orgId))
      .then((drivers: IDriver[]) => dispatch(getVehiclesSuccess(drivers)))
      .catch((error) => dispatch(getVehicleError(error.message)));
};
