import { Reducer } from "redux";
import { createReducer } from "@reduxjs/toolkit";
import { getVehiclesSuccess, getVehicleError } from "./actions";
import { IVehicle } from "../../common/@types/vehicle";

export interface State {
  vehicles: IVehicle[];
  error: string;
}

const initialState: State = {
  vehicles: [],
  error: "",
};

const reducer: Reducer<State> = createReducer(initialState, {
  [getVehiclesSuccess.type]: (state, action) => {
    state.vehicles = action.payload;
  },
  [getVehicleError.type]: (state, action) => {
    state.error = action.payload;
  },
});

export default reducer;
