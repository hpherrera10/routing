import { Reducer } from "redux";
import { createReducer } from "@reduxjs/toolkit";
import {
  getRoutesError,
  getRoutesSuccess,
  putRouteError,
  putRouteSuccess,
} from "./actions";
import { IRoute } from "../../common/@types/route";

export interface State {
  routes: IRoute[];
  error: string;
  message: string;
}

const initialState: State = {
  routes: [],
  error: "",
  message: "",
};

const reducer: Reducer<State> = createReducer(initialState, {
  [getRoutesSuccess.type]: (state, action) => {
    state.routes = action.payload;
  },
  [getRoutesError.type]: (state, action) => {
    state.error = action.payload;
  },
  [putRouteSuccess.type]: (state, action) => {
    state.message = `Ruta ${action.payload.id} actualizada, drivers ${action.payload.driver_id}`;
  },
  [putRouteError.type]: (state) => {
    state.message = "La ruta no se pudo actualizar";
  },
});

export default reducer;
