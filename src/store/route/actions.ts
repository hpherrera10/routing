import { createAction, remoteCall } from "../utils";
import { RouteServ } from "../../services";
import { IRoute } from "../../common/@types/route";

export const getRoutesSuccess = createAction("route/getRoutesSuccess");
export const getRoutesError = createAction("route/getRoutesError");
export const putRouteSuccess = createAction("route/putRouteSuccess");
export const putRouteError = createAction("route/putRouteError");

export const getRoutes = (orgId: number) => {
  return (dispatch: any) =>
    remoteCall(dispatch, "Route", () => RouteServ.getRoutes(orgId))
      .then((routes: IRoute[]) => dispatch(getRoutesSuccess(routes)))
      .catch((error) => dispatch(getRoutesError(error.message)));
};

export const putRoute = (routeId: number, route: any) => {
  return (dispatch: any) =>
    remoteCall(dispatch, "Route", () => RouteServ.putRoute(routeId, route))
      .then((response: string) => dispatch(putRouteSuccess(response)))
      .catch((error) => dispatch(putRouteError(error.message)));
};
