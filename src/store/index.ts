import { configureStore } from "@reduxjs/toolkit";

import { driverReducer } from "./driver";
import { organizationReducer } from "./organization";
import { remoteReducer } from "./remote";
import { routeReducer } from "./route";
import { vehicleReducer } from "./vehicle";

const reducer = {
  driver: driverReducer,
  organization: organizationReducer,
  remote: remoteReducer,
  route: routeReducer,
  vehicle: vehicleReducer,
};

const store = configureStore<any, any>({
  reducer,
});

export default store;
