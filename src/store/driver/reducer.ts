import { Reducer } from "redux";
import { createReducer } from "@reduxjs/toolkit";
import { getDriverSuccess, getDriverError } from "./actions";
import { IDriver } from "../../common/@types/driver";

export interface State {
  drivers: IDriver[];
  error: string;
}

const initialState: State = {
  drivers: [],
  error: "",
};

const reducer: Reducer<State> = createReducer(initialState, {
  [getDriverSuccess.type]: (state, action) => {
    state.drivers = action.payload;
  },
  [getDriverError.type]: (state, action) => {
    state.error = action.payload;
  },
});

export default reducer;
