import { createAction, remoteCall } from "../utils";
import { DriverServ } from "../../services";
import { IDriver } from "../../common/@types/driver";

export const getDriverSuccess = createAction("driver/getDriversSuccess");
export const getDriverError = createAction("driver/getDriversError");

export const getDrivers = (orgId: number) => {
  return (dispatch: any) =>
    remoteCall(dispatch, "Driver", () => DriverServ.getDrivers(orgId))
      .then((drivers: IDriver[]) => dispatch(getDriverSuccess(drivers)))
      .catch((error) => dispatch(getDriverError(error.message)));
};
