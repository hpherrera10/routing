import { createAction, remoteCall } from "../utils";
import { OrganizationServ } from "../../services";
import { IOrganization } from "../../common/@types/organization";

export const getOrganizationsSuccess = createAction(
  "organization/getOrganizationsSuccess"
);
export const getOrganizationsError = createAction(
  "organization/getOrganizationsError"
);
export const setOrganizationsSuccess = createAction(
  "organization/setOrganizationsSuccess"
);
export const setOrganizationsError = createAction(
  "organization/setOrganizationsError"
);

export const getOrganizations = () => {
  return (dispatch: any) =>
    remoteCall(dispatch, "Organization", () =>
      OrganizationServ.getOrganizations()
    )
      .then((organizations: IOrganization[]) =>
        dispatch(getOrganizationsSuccess(organizations))
      )
      .catch((error) => dispatch(getOrganizationsError(error.message)));
};

export const getOrganization = (orgId: number) => {
  return (dispatch: any) =>
    remoteCall(dispatch, "Organization", () =>
      OrganizationServ.getOrganization(orgId)
    )
      .then((organization: IOrganization) =>
        dispatch(setOrganizationsSuccess(organization))
      )
      .catch((error) => dispatch(setOrganizationsError(error.message)));
};
