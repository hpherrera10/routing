import { Reducer } from "redux";
import { createReducer } from "@reduxjs/toolkit";
import {
  getOrganizationsSuccess,
  getOrganizationsError,
  setOrganizationsError,
  setOrganizationsSuccess,
} from "./actions";
import { IOrganization } from "../../common/@types/organization";

export interface State {
  organizations: IOrganization[];
  organization: IOrganization;
  error: string;
}

const initialState: State = {
  organizations: [],
  organization: { id: 0, name: "" },
  error: "",
};

const reducer: Reducer<State> = createReducer(initialState, {
  [getOrganizationsSuccess.type]: (state, action) => {
    state.organizations = action.payload;
  },
  [getOrganizationsError.type]: (state, action) => {
    state.error = action.payload;
  },
  [setOrganizationsSuccess.type]: (state, action) => {
    state.organization = action.payload;
  },
  [setOrganizationsError.type]: (state, action) => {
    state.error = action.payload;
  },
});

export default reducer;
