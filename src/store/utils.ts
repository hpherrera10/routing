import { loading } from "./remote/actions";
import { Dispatch } from "redux";
import { createAction as reduxCreateAction } from "@reduxjs/toolkit";

export const createAction = <T = any>(type: string) =>
  reduxCreateAction<T>(type);

export const remoteCall = (
  dispatch: Dispatch<any>,
  context: string,
  call: () => Promise<any>
) => {
  dispatch(loading({ loading: true, context }));
  return call()
    .then((success) => {
      dispatch(loading({ loading: false, context }));
      return success;
    })
    .catch((error) => {
      dispatch(loading({ loading: false, context }));
      throw error;
    });
};
