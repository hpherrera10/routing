import { Theme, createStyles, makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      padding: 16,
    },
    select: {
      display: "flex",
      flexDirection: "row",
      alignItems: "center",
      marginBottom: 16,
    },
    list: {
      marginBottom: 16,
    },
    routes: {
      marginTop: 16,
    },
    title: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
    },
    button: {
      marginLeft: 16,
    },
    filter: {
      marginTop: 16,
      marginBottom: 32,
    },
  })
);

export default useStyles;
