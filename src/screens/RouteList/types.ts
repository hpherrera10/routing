import { IDriver } from "../../common/@types/driver";
import { IOrganization } from "../../common/@types/organization";
import { IRoute } from "../../common/@types/route";
import { IVehicle } from "../../common/@types/vehicle";

export interface State {
  organization: any;
}

export interface StateProps {
  organizations: IOrganization[];
  organization: IOrganization;
  drivers: IDriver[];
  vehicles: IVehicle[];
  routes: IRoute[];
  loading: boolean;
  message: string;
}

export interface DispatchProps {
  loadOrganizations: () => void;
  loadRoutes: (orgId: number) => void;
  loadDrivers: (orgId: number) => void;
  loadVehicles: (orgId: number) => void;
  handleUpdate: (routeId: number, route: any) => void;
}

export interface Props extends StateProps, DispatchProps {}
