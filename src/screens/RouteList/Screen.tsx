import { useState, useEffect } from "react";
import { Box, Button, Grid, Typography } from "@material-ui/core";

import Select from "../../components/Select";
import List from "../../components/List";
import Filter from "../../components/Filter";
import Title from "../../components/Title";

import { Props, State } from "./types";
import useStyles from "./styles";
import SimpleMap from "../../components/Map";
import Schedule from "../../components/Schedule";

function RouteList(props: Props) {
  const {
    organizations,
    organization,
    drivers,
    vehicles,
    routes,
    message,
    loading,
    loadOrganizations,
    loadRoutes,
    loadDrivers,
    handleUpdate,
    loadVehicles,
  } = props;

  const organizationSaved = window.localStorage.getItem("organization");
  const initialState: State = {
    organization:
      organization?.id ||
      (organizationSaved && JSON.parse(organizationSaved).id),
  };
  const [state, setState] = useState(initialState);

  const styled = useStyles();

  useEffect(() => {
    getLoadOrganizations();
  }, []);

  useEffect(() => {
    getLoadRoute();
    getLoadDrivers();
    getVehicles();
  }, [state.organization, message]);

  const getLoadOrganizations = () => {
    loadOrganizations();
  };

  const getLoadRoute = () => {
    state.organization && loadRoutes(state.organization);
  };

  const getLoadDrivers = () => {
    state.organization && loadDrivers(state.organization);
  };

  const getVehicles = () => {
    state.organization && loadVehicles(state.organization);
  };

  const renderSelectionOrg = () => (
    <div className={styled.select}>
      <Typography> Organización: </Typography>
      <Select
        list={organizations}
        selection={state.organization}
        handleSelect={(value) => setState({ ...state, organization: value })}
      />
    </div>
  );

  const renderList = () => (
    <div className={styled.list}>
      <Typography variant="body2"> Lista de rutas </Typography>
      {routes.length > 0 && <Title />}
      {routes.length > 0 && <Filter />}
      <div className={styled.routes}>
        <List
          list={routes}
          vehicles={vehicles}
          drivers={drivers}
          handleUpdate={(routeId: number, route: any) =>
            handleUpdate(routeId, route)
          }
        />
      </div>
    </div>
  );

  return (
    <div className={styled.container}>
      {renderSelectionOrg()}
      <Grid container spacing={2}>
        <Grid item xs={8}>
          {renderList()}
        </Grid>
        <Grid item xs={4}>
          {routes.length > 0 && <SimpleMap />}
        </Grid>
      </Grid>
      {routes.length > 0 && <Schedule routes={routes} />}
    </div>
  );
}

export default RouteList;
