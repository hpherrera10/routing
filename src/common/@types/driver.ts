export interface IDriver {
  id: number;
  name: string;
  last_name: string;
  vehicle: string;
}
