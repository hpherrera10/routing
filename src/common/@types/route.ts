export interface IRoute {
  id: number;
  starts_at: string;
  ends_at: string;
  travel_time: string;
  total_stops: number;
  action: string;
  driver_id: string;
  organization_id: string;
}
