# routing


## Getting started

The main routes for the project are:

- `/src` Content management this project
- `/src/common` Content types
- `/src/components`
- `/src/containers` Receive **Redux** state updates and dispatch actions
- `/src/screens`
- `/src/services`
- `/src/store` Redux store

## Installing / Getting started

### Pre requisites

- [Yarn](https://yarnpkg.com/en/)
- [React](https://reactjs.org/)
- [Redux](https://redux.js.org/)

```shell
npm install -g yarn
clone repository
yarn install

or 

npm install

yarn start
```
### Image

![image.png](./image.png)

